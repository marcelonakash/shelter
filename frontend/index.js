import React, {useState, useEffect, useCallback} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import ReactDOM from 'react-dom';
import Navigation from './components/Navigation';
import './styles/main.scss';
import Animals from "./components/Animals";
import PetContext from "./context/PetContext";
import './styles/Index.scss'

const Home = () => {

  const [pet, setPet] = useState({})
  const [pets, setPets] = useState([])
  const [alert, setAlert] = useState("")

  useEffect(() => {
    fetch('http://localhost:3000/pets')
      .then(response => response.json())
      .then(data => setPets(data));
  }, []);

  return <>
    <PetContext.Provider value={{pet, setPet, pets, setPets, alert, setAlert}}>
      <Navigation />
      <div className={'alert alert-error alert-danger' + (!!alert ? '' : ' d-none')}>{alert}</div>
      <Animals />
    </PetContext.Provider>
  </>
};

ReactDOM.render(
  <Home />,
  document.getElementById('root')
);
