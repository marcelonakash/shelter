import {createContext} from "react";

const PetContext = createContext({
    pet: {},
    setPet: (id) => {},
    pets: [],
    setPets: (data) => {},
    alert: "",
    setAlert: (msg) => {}
})

export default PetContext