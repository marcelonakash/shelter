function fetchData() {
    return fetch.apply(null, arguments).then(response => {
        if (!response.ok) {
            let err = new Error("HTTP status code: " + response.status)
            err.response = response
            err.status = response.status
            throw err
        }
        return response
    })
}

module.exports = {fetchData}