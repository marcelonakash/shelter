import React, {useEffect, useState} from "react";
import PetContext from "../context/PetContext";
import "../styles/AnimalModal.scss"
import {fetchData} from "../util/util";

const AnimalModal = () => {

    const [errors,setErrors] = useState({});
    const {pet, setPet, pets, setPets, setAlert} = React.useContext(PetContext)
    const handleHide = () => {
        setPet({})
        setErrors({})
    }

    const handleName = (e) => {setPet({...pet,name: e.target.value})}
    const handleType = (e) => setPet({...pet,type: e.target.value})
    const handleArrived = (e) => setPet({...pet,arrived: e.target.value})
    const handleAdopter = (e) => setPet({...pet,adopter: e.target.value})
    const handleAdoptedAt = (e) => setPet({...pet,adopted_at: e.target.value})

    const validation = async () => {
        const validation = {}
        if(new Date(pet.arrived).getTime() > new Date().getTime())
            validation['arrived'] = 'Arrived cannot be greater than today';
        if(!!pet.adopted_at && new Date(pet.adopted_at).getTime() > new Date().getTime())
            validation['adopted_at'] = 'Adopted at cannot be greater than today'

        setErrors(validation)

        return Object.keys(validation).length == 0
    }

    const createPet = () => {

        fetchData("http://localhost:3000/pets",
            {
                method: "POST",
                "headers": {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify( pet )
            })
            .then(res => res.json())
            .then(data => {
                setPets([...pets,data])
                handleHide()
            }).catch(err => {
            console.log(err)
            setAlert("Unable to create pet")
            setTimeout(function () {setAlert("")},3000)
        })
    }

    const updatePet = () => {
        fetchData("http://localhost:3000/pets/"+pet.id,
            {
                method: "PUT",
                "headers": {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify( pet )
            })
            .then(res => res.json())
            .then(data => {
                setPets(pets.map(el => {
                    if(el.id == pet.id)
                        return pet
                    return el
                }))
                handleHide()
            }).catch(err => {
            setAlert("Unable to update pet")
            setTimeout(function () {setAlert("")},3000)
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault()

        if(!await validation())
            return

        if(pet.id === -1) {
            createPet()
            return
        }

        updatePet()
    }

    return (
        <div className={'modal-dialog'+ (!!pet.id ? ' d-block' : '')}>
            <form onSubmit={handleSubmit}>
                <div className='modal-content'>
                    <div className='modal-header'>
                        <div className='modal-title h4'> {pet.id == -1 ? 'Add new' : 'Edit'} Pet</div>
                    </div>
                    <div className='modal-body'>
                        <div className='form-group'>
                            <label>Name</label>
                            <input id='name' className='form-control' onChange={handleName} value={pet.name || ''} required/>
                        </div>
                        <div className='form-group'>
                            <label>Type</label>
                            <select id='type' className='form-control' onChange={handleType} value={pet.type || ''} required>
                                <option value=''>...select...</option>
                                <option value='cat'>cat</option>
                                <option value='dog'>dog</option>
                            </select>
                        </div>
                        <div className='form-group'>
                            <label>Arrived</label>
                            <input id='arrived' className='form-control' onChange={handleArrived} value={pet.arrived || ''} type='date' required/>
                            <span className='alert-danger alert-arrived'>{errors['arrived'] || ''}</span>
                        </div>
                        <div className='form-group'>
                            <label>Adopted At</label>
                            <input id='adopted_at' className='form-control' onChange={handleAdoptedAt} value={!!pet.adopted_at ? pet.adopted_at : ''}  type='date' required={!!pet.adopter}/>
                            <span className='alert-danger alert-adopted'>{errors['adopted_at'] || ''}</span>
                        </div>
                        <div className='form-group'>
                            <label>Adopter</label>
                            <input id='adopter' className='form-control' onChange={handleAdopter} value={!!pet.adopter ? pet.adopter : ''} required={!!pet.adopted_at}/>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button id='close' type="button" onClick={handleHide} className="btn btn-secondary">Close</button>
                        <button id='save' type="submit" className="btn btn-primary">Save Changes</button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default AnimalModal