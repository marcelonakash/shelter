import React, {useState} from 'react'
import Item from './Item'
import AnimalModal from "./AnimalModal";
import PetContext from "../context/PetContext";

const Animals = () => {

    const [animalType, setAnimalType] = useState("")
    const [adopted, setAdopted] = useState("")
    const {pets, setPet, pet} = React.useContext(PetContext)

    const handleAdd = () => {
        setPet({...pet,id: -1})
    }

    const search = arr => arr.filter(
        element => {
            const adoptedFilter = !!adopted && (
                (adopted == 'not_adopted' && !!element.adopted_at == false)
                || (adopted == 'adopted' && !!element.adopted_at)
            ) || !!adopted == false;

            const animalTypeFilter = !!animalType && animalType == element.type || !!animalType == false;

            return adoptedFilter && animalTypeFilter;
        })

    return (
        <>
            <div className='container'>
                <div className='row'>
                    <div className='col-md-6'>
                        <label>Animal Type: </label>
                        <select id='filter-type' className='form-select' onChange={e => setAnimalType(e.target.value)}>
                            <option value=''>All</option>
                            <option value='cat'>Cat</option>
                            <option value='dog'>Dog</option>
                        </select>
                    </div>
                    <div className='col-md-6'>
                        <label> Adoption: </label>
                        <select id='filter-adoption' className='form-select' onChange={e => setAdopted(e.target.value)}>
                            <option value=''>All</option>
                            <option value='adopted'>Adopted</option>
                            <option value='not_adopted'>Not adopted</option>
                        </select>
                    </div>
                    <h5 className='text-center showing'>Showing {search(pets).length} pets</h5>
                    <div className='float-end'>
                        <button className='float-end btn btn-primary mb-3' id='add-pet' onClick={handleAdd}>Add new pet</button>
                    </div>
                </div>
                {search(pets).map((animal, key) => <Item key={key} animal={animal}/>)}
            </div>
            <AnimalModal />
        </>
    )
}

export default Animals;