import React, {useContext} from 'react';
import '../styles/Item.scss';
import cat from '../images/cat.png';
import dog from '../images/dog.png';
import PetContext from "../context/PetContext";
import {fetchData} from "../util/util";

const inDays = (arrived, adopted_at) => {
    const baseTime = adopted_at ? new Date(adopted_at).getTime() : new Date().getTime();
    const timeDiff = baseTime - new Date(arrived).getTime();
    return (adopted_at ? 'stayed ' : '' ) + Math.floor(timeDiff/(24*3600*1000))
}

const adoptedBy = (adopted_at, adopter) => adopted_at ? ' - adopted by '+ adopter + ' on ' + (new Date(adopted_at).toLocaleDateString()) : ''

const Item = ({ animal }) => {

    const {pet, pets,setPet, setPets, setAlert} = useContext(PetContext)

    const selectPet = (e) => {
        setPet(pets.find(el => el.id == e.target.value))
    }

    const deletePet = (e) => {
        if(confirm("Do you really want to delete this pet?")) {
            fetchData("http://localhost:3000/pets/"+e.target.value, {method: "DELETE"})
            .then(res => res.json())
            .then(data => {
                setPets(pets.filter(el => el.id != e.target.value))
            }).catch(err => {
                setAlert("Unable to delete pet")
                setTimeout(function () {setAlert("")},3000)
            })
        }
    }

    return (
        <ul className='list-group'>
            <li className={'list-group-item ' + (animal.adopter ? 'adopted' : '')}>
                <span>
                    <h4>{animal.id}
                        <img src={animal.type == 'cat' ? cat : dog} className='item-animal-icon'/>
                        {animal.name} <small>{adoptedBy(animal.adopted_at, animal.adopter)}</small>
                    </h4>
                    <b>{inDays(animal.arrived, animal.adopted_at)}</b> days on shelter <b>{animal.shelter}</b>
                </span>
                <button id='delete' className='btn btn-danger float-end' value={animal.id} onClick={deletePet}>Delete</button>
                <button id='edit' className='btn btn-warning float-end mx-2' value={animal.id} onClick={selectPet}>Edit</button>
            </li>
        </ul>
    )
}
export default Item;