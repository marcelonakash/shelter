 const Pet = {
    validate: (data) => {
        const errors = {}
        const today = new Date().getTime()

        if(! !!data.name)
            errors['name'] = 'missing name'

        if(! !!data.type)
            errors['type'] = 'missing pet type'

        if(! !!data.arrived)
            errors['arrived'] = 'missing arrived date'

        if(!!data.arrived && new Date(data.arrived).getTime() > today)
            errors['arrived'] = 'invalid arrived date'

        if(! !!data.adopted_at && !!data.adopter)
            errors['adopted_at'] = 'missing adopted at date'

        if(!!data.adopted_at && new Date(data.adopted_at).getTime() > today)
            errors['adopted_at'] = 'invalid adopted at date'

        if(!!data.adopted_at && ! !!data.adopter)
            errors['adopter'] = 'missing adopter'

        return errors
    }
}

module.exports = Pet