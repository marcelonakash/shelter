const express    = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const port = 3000

const Pet = require("./services/pet")

const app = express();
app.use(cors())

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.get('/pets', (req, res) => {
  res.json([{
    id: 1001,
    type: 'cat',
    name: 'Vasia',
    arrived: '2022-02-01',
    adopted_at: null,
    adopter: null,
    shelter: 'Main'
  },{
    id: 1002,
    type: 'dog',
    name: 'Musci',
    arrived: '2022-01-10',
    adopted_at: '2022-01-20',
    adopter: 'John',
    shelter: 'Main'
  }
  ])
})

app.post('/pets', (req, res) => {

    const errors = Pet.validate(req.body)

    if(Object.keys(errors).length > 0) {
      res.status(400)
      res.json(errors)
      return
    }

    const id = Math.floor(Math.random() * (9999 - 2000 + 1) + 2000)

    res.status(201)
    res.json({
      id: id,
      name: req.body.name,
      type: req.body.type,
      arrived: req.body.arrived,
      adopted_at: req.body.adopted_at || null,
      adopter: req.body.adopter || null,
      shelter: 'Main'
    })
})

app.put('/pets/:id', (req, res) => {
    const errors = Pet.validate(req.body)
    if(Object.keys(errors).length > 0) {
        res.status(400)
        res.json(errors)
        return
    }
    res.json(req.body)
})

app.delete('/pets/:id', (req, res) => {
    res.send(req.body)
})

app.listen(port, () => console.log(`server is listening on port ${port}!`))