describe('pet filter', () => {

    const defaultPets = 2
    const defaultCatNumber = 1
    const defaultDogAdoptedNumber = 1

    before(() => {
        cy.visit('/')
    })

    it('should filter pets by type and adoption status', () => {
        cy.get('.showing').contains("Showing "+defaultPets+" pets")
        cy.get('#filter-type').select('cat')
        cy.get('.showing').contains("Showing "+defaultCatNumber+" pets")
        cy.get('.list-group').its('length').should('eq', defaultCatNumber)

        cy.get('#filter-type').select('dog')
        cy.get('#filter-adoption').select('adopted')
        cy.get('.showing').contains("Showing "+(defaultPets-defaultCatNumber)+" pets")
        cy.get('.list-group').its('length').should('eq', defaultDogAdoptedNumber)
    })
})