describe('pet crud', () => {

    const currentDate = new Date().toISOString().split('T').shift()

    before(() => {
        cy.visit('/')
    })

    it('should validate form', () => {

        const tomorrow = new Date().setDate(new Date().getDate() + 1)
        const tomorrowDate = new Date(tomorrow).toISOString().split('T').shift()

        cy.get("#add-pet").click()

        cy.get('#save').click()
        cy.get('#name:invalid').should('have.length',1)
        cy.get('#name').type('new pet')
        cy.get('#save').click()
        cy.get('#name:invalid').should('have.length',0)

        cy.get('#type:invalid').should('have.length',1)
        cy.get('#type').select('dog')
        cy.get('#save').click()
        cy.get('#type:invalid').should('have.length',0)

        cy.get('#arrived:invalid').should('have.length',1)
        cy.get('#arrived').type(tomorrowDate)
        cy.get('#adopted_at').type(tomorrowDate)
        cy.get('#save').click()
        cy.get('#arrived:invalid').should('have.length',0)

        cy.get('#adopter:invalid').should('have.length',1)
        cy.get('#adopter').type('John')
        cy.get('#save').click()

        cy.get('.alert-arrived').should('be.visible')
        cy.get('.alert-adopted').should('be.visible')

        cy.get('#close').click()
    })

    it("should create pet", () => {
        const petName = 'New pet';
        const adopter = 'Peter';
        cy.get('.list-group').its('length').then(length => {
            cy.get("#add-pet").click()
            cy.get('#name').type(petName)
            cy.get('#type').select('dog')
            cy.get('#arrived').type(currentDate)
            cy.get('#adopted_at').type(currentDate)
            cy.get('#adopter').type(adopter)
            cy.get('#save').click()

            cy.get('.list-group').its('length').should('be.greaterThan',length)

            cy.get('.list-group:last').contains(petName)
            cy.get('.list-group:last').contains('adopted by '+adopter)
            cy.get('.list-group:last').contains('stayed 0 days on shelter')
        })
    })

    it("should edit pet", () => {
        cy.get('.list-group:first').find('#edit').click()
        const newName = 'editing name';
        cy.get('#name').clear()
        cy.get('#name').type(newName)
        cy.get('#save').click()
        cy.get('.list-group:first').contains(newName)
    })

    it("should delete pet", () => {
        cy.get('.list-group').its('length').then(length => {
            cy.get('.list-group:first').find('#delete').click()
            cy.get('.list-group').its('length').should('be.lessThan',length)
        })
    })
})

